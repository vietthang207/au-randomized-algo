//#include <bits/stdc++.h>
//#include <stdio.h>
#include <bit>
#include <bitset>
#include <iostream>
#include <random>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

typedef uint64_t u64;
typedef vector<uint64_t> vu64;

const u64 MAX_U64 = 1UL << 63 - 1 + 1UL << 63;
const int DOMAIN_SIZE = 1 << 25;
const int HASH_PARAM_W = 64;
const int PERFECT_HASH_PARAM_W = 64;
const int PERFECT_HASH_PARAM_L = 25;
const int LINEAR_HASH_PARAM_W = 64;
const int LINEAR_HASH_PARAM_L = 25;
const int UNDEFINED = -1;

random_device rd;
mt19937_64 rng(rd());

u64 rand_odd_u64(){
    // Return a random unsigned 64-bit odd int
    u64 random_number = (rng()/2)*2+1;
    return random_number;
}

u64 multiply_shift(u64 x, u64 param_a, int param_w, int param_l){
    //multi-shift hash function
    u64 h = (x*param_a) >> (param_w-param_l);
    return h;
}

class ChainingHashTable {
    protected:
        u64 param_m;
        u64 param_l;
        u64 param_a;
        // u64 max_bucket_size;

        vector<vu64*> key_buckets;
        vector<vu64*> val_buckets;

        u64 hash(u64 e) {
            u64 h = multiply_shift(e, param_a, HASH_PARAM_W, param_l);
            return h;
        }

    public:
        ChainingHashTable(u64 n) {
            param_m = bit_ceil(n/2);
            param_l = log2(param_m);
            param_a = rand_odd_u64();
 
            for (int i = 0; i< param_m; i++) {
                key_buckets.push_back(nullptr);
                val_buckets.push_back(nullptr);
            }
            return;
        }

        void update(u64 key, u64 delta) {
            u64 h = hash(key);
            // cout<<"hash: "<<h<<endl;
            if (key_buckets[h] == nullptr) {
                //First member of this bucket, so initialize the bucket
                // cout << "first member"<<endl;
                key_buckets[h] = new vu64();
                val_buckets[h] = new vu64();
            }
            
            int index = -1;
            auto it = find(key_buckets[h]->begin(), key_buckets[h]-> end(), key);
            if (it == key_buckets[h]->end()) {
                //Key not found
                // cout << "key not found" <<endl;
                key_buckets[h]->push_back(key);
                val_buckets[h]->push_back(0);
                index = key_buckets[h]->size()-1;
            } else {
                index = it - key_buckets[h]->begin();
            }
            val_buckets[h]->at(index) += delta;
            return;
        }

        u64 query() {
            u64 result = 0;
            for (int i = 0; i< param_m; i++) {
                if (key_buckets[i] != nullptr) {
                    for (int j = 0; j< key_buckets[i]->size(); j++) {
                        u64 val = val_buckets[i]->at(j);
                        result += val * val;
                    }
                }
            }
            return result;
        }
};
#include <iostream>
#include <random>
#include <vector>
#include "sketch.cpp"
#include "chaining_hash.cpp"

using namespace std;

const long long NUM_UPDATES = pow(10, 3);
const long long N_MIN = 6;
const long long N_MAX = 28;

long long get_norm_from_sketch(HashFunctionAlgo algo, long long logr) {
    long long r = pow(2, logr);
    Sketch s(algo, r);
    for (long long i = 0; i < NUM_UPDATES; i++){
        s.update(i, i*i);
    }
    return s.query();
}

long long get_norm_from_chaining_hash() {
    ChainingHashTable ht(NUM_UPDATES);
    for (long long i = 0; i < NUM_UPDATES; i++){
        ht.update(i, i*i);
    }
    return ht.query();
}

int main(int argc, char* argv[]){
    //Exercise 8
    double true_norm= 0;
    for (int i = 0; i< 1000; i++) {
        true_norm += pow(i, 4);
    }
    // double true_norm = get_norm_from_chaining_hash();

    cout<<"result for cubic hash"<<endl;
    cout<<"log(r),avg_error"<<endl;
    for (long long logr = 3; logr < 21; logr++) {
        double sum = 0;
        for (int i =0; i<1000; i++) {
            double estimated_norm = get_norm_from_sketch(CUBIC_HASH, logr);
            sum += estimated_norm;
        }
        double avg_estimated_norm = sum/1000;
        // cout << estimated_norm << " " << true_norm <<endl;
        // cout<<logr<<","<< abs(avg_estimated_norm-true_norm)/true_norm <<endl;
        printf("%2lld, %f\n", logr, abs(avg_estimated_norm-true_norm)/true_norm);
    }

    cout<<"log(r),max_error"<<endl;
    for (long long logr = 3; logr < 21; logr++) {
        double max_err = 0;
        for (int i =0; i<1000; i++) {
            double estimated_norm = get_norm_from_sketch(CUBIC_HASH, logr);
            max_err = max(max_err, abs((estimated_norm-true_norm)/true_norm));
        }
        // cout << estimated_norm << " " << true_norm <<endl;
        // cout<<logr<<","<< abs(avg_estimated_norm-true_norm)/true_norm <<endl;
        printf("%2lld, %f\n", logr, max_err);
    }

    cout<<"result for mul shift"<<endl;
    cout<<"log(r),avg_error"<<endl;
    for (long long logr = 3; logr < 21; logr++) {
        double sum = 0;
        for (int i =0; i<1000; i++) {
            double estimated_norm = get_norm_from_sketch(MULTIPLY_SHIFT, logr);
            sum += estimated_norm;
        }
        double avg_estimated_norm = sum/1000;
        // cout << estimated_norm << " " << true_norm <<endl;
        // cout<<logr<<","<< abs(avg_estimated_norm-true_norm)/true_norm <<endl;
        printf("%2lld, %f\n", logr, abs(avg_estimated_norm-true_norm)/true_norm);
    }

    cout<<"log(r),max_error"<<endl;
    for (long long logr = 3; logr < 21; logr++) {
        double max_err = 0;
        for (int i =0; i<1000; i++) {
            double estimated_norm = get_norm_from_sketch(MULTIPLY_SHIFT, logr);
            max_err = max(max_err, abs((estimated_norm-true_norm)/true_norm));
        }
        // cout << estimated_norm << " " << true_norm <<endl;
        // cout<<logr<<","<< abs(avg_estimated_norm-true_norm)/true_norm <<endl;
        printf("%2lld, %f\n", logr, max_err);
    }
}
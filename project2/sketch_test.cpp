#include <iostream>
#include <random>
#include <vector>
#include <chrono>
#include "sketch.cpp"
#include "chaining_hash.cpp"

using namespace std;

typedef uint64_t u64;
typedef vector<uint64_t> vu64;

const double NUM_UPDATES = pow(10, 9);
const u64 N_MIN = 6;
const u64 N_MAX = 28;

void test_sketch(HashFunctionAlgo algo, u64 logr) {
    Sketch s(algo, pow(2, logr));
    for (u64 N = N_MIN; N <= N_MAX; N++){
        u64 n = pow(2, N);
        long long timesum = 0;
        auto start = std::chrono::high_resolution_clock::now();
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);
        // auto start = clock();
        start = std::chrono::high_resolution_clock::now();
        for (u64 i = 0; i < NUM_UPDATES; i++){
            s.update(i % n, 1);
        }
        stop = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);
        timesum = duration.count();
        // auto stop = clock();
        // cout << "2^" << N << "total time: " << time << " microseconds" << endl;
        // cout << "2^" << N << "avg time pr n: " << time/pow(10, 9) << " microseconds" << endl;
        if (algo == CUBIC_HASH) {
            cout<<"cubic_hash";
        } else if (algo == MULTIPLY_SHIFT) {
            cout<<"multiply_shift";
        }
        cout << "," << logr << "," << N << "," << timesum / NUM_UPDATES <<endl;
    }
}

void test_chaining_hash() {
    for (u64 N = N_MIN; N <= N_MAX; N++){
        u64 n = pow(2, N);
        ChainingHashTable ht(n);
        long long timesum = 0;
        auto start = std::chrono::high_resolution_clock::now();
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);
        // auto start = clock();
        start = std::chrono::high_resolution_clock::now();
        for (u64 i = 0; i < NUM_UPDATES; i++){
            ht.update(i % n, 1);
        }
        stop = std::chrono::high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);
        timesum = duration.count();
        // auto stop = clock();
        // auto time = stop - start;
        // cout << "2^" << N << "total time: " << time << " microseconds" << endl;
        // cout << "2^" << N << "avg time pr n: " << time/pow(10, 9) << " microseconds" << endl;

        cout<< "chaining_hash";
        cout << "," << 0 << "," << N << "," << timesum / NUM_UPDATES <<endl;
    }
}

int main(int argc, char* argv[]){
    //Exercise 7

    cout << "algo, log(r), N, avg_time"<<endl;

    test_chaining_hash();

    test_sketch(CUBIC_HASH, 7);
    test_sketch(CUBIC_HASH, 10);
    test_sketch(CUBIC_HASH, 20);

    test_sketch(MULTIPLY_SHIFT, 7);
    test_sketch(MULTIPLY_SHIFT, 10);
    test_sketch(MULTIPLY_SHIFT, 20);

}
#include <iostream>
#include <random>
#include <vector>
#include <chrono>
#include "hash_functions.cpp"

using namespace std;

typedef uint64_t u64;
typedef vector<uint64_t> vu64;


const long long NUM_TESTS= pow(10, 6);

int main(int argc, char* argv[]){
    MultiplyShift h;
    CubicHash g;
    long long timesum = 0;

    auto start = std::chrono::high_resolution_clock::now();
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);

    start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < NUM_TESTS; ++i){
        h.hash(i);
    }
    stop = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);
    timesum = duration.count();

    cout << "total time: "<<timesum <<" nanosecs"<< endl;
    cout << "avg time: "<<timesum * 1.0 / NUM_TESTS<< " nanosecs"<<endl;

    timesum = 0;
    
    start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < NUM_TESTS; ++i){
        g.hash_h_and_g(i);
    }
    stop = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);
    timesum += duration.count();
    
    cout << "total time: "<<timesum <<" nanosecs"<< endl;
    cout << "avg time: "<<timesum * 1.0 / NUM_TESTS<< " nanosecs"<<endl;

    return 0;
}
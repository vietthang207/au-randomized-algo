#include <iostream>
#include <random>
#include <vector>
#include "hash_functions.cpp"

using namespace std;

typedef vector<long long> vll;

class Sketch{
    protected:
        HashFunctionAlgo algo;
        HashFunction* h;
        long long r;
        vll A;

    public:
    //constructors
    Sketch(HashFunctionAlgo algo, long long r){
        this->algo = algo;
        this->r = r;
        this->A = vll(r, 0);
        if (algo == MULTIPLY_SHIFT) {
            this->h = new MultiplyShift(r);
        } else if (algo == CUBIC_HASH) {
            this->h = new CubicHash(r);
        }
    }

    void update(long long i, long long delta){
        // cout<<"update: "<<i<<" "<<delta<<endl;
        pair<long long, long long> h_g = h->hash_h_and_g(i);
        // if (algo == MULTIPLY_SHIFT) {
        //     h_g = h->hash_h_and_g(i);
        // } else if (algo == CUBIC_HASH) {
        //     h_g = h->hash_h_and_g(i);
        // }
        long long h_i = h_g.first;
        long long g_i = h_g.second;
        // if (A[h_i] != 0) {
            // cout<<h_i<<" "<<g_i<<endl;
            // cout<<"before "<<A[h_i]<<endl;
        // }
        A[h_i] = A[h_i] + g_i * delta;
        // cout<<"after "<<A[h_i]<<endl;
    }

    long long query(){
        long long sum = 0;
        for (long long i = 0; i < r; i++){
            sum += pow(A[i], 2);
            // if (A[i] != 0) cout<<A[i]<<" ";
        }
        // cout<<endl<<sum<<endl;
        return sum;
    }
};
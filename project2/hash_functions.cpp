#include <iostream>
#include <random>
#include <tuple>
#include <vector>


using namespace std;

typedef uint64_t u64;
enum HashFunctionAlgo {MULTIPLY_SHIFT, CUBIC_HASH};

const long long DEFAULT_P = pow(2, 31) -1;
const long long DEFAULT_R = pow(2, 20);

class HashFunction{
    protected:
        HashFunctionAlgo algo;

    public:
        virtual long long hash(long long x) {
            return 0;
        }

        virtual pair<long long,long long> hash_h_and_g(long long x){
            return pair<long long, long long>(0, 0);
        }

};

class MultiplyShift: public HashFunction {
    protected:
        u64 param_a, param_w, param_l;
        long long param_r;
    
    public:
        //constructor
        MultiplyShift(u64 param_w, u64 param_l, long long param_r){
            this->algo = MULTIPLY_SHIFT;
            this->param_w = param_w;
            this->param_l = param_l;
            this->param_r = param_r;

            random_device rd;
            mt19937 rng(rd());
            this->param_a = (rng()/2)*2+1;
        }

        MultiplyShift(long long r): MultiplyShift(64, 31, r){}

        MultiplyShift():MultiplyShift(64, 31, DEFAULT_R) {}

        long long hash(long long x) {
            long long h = (x*param_a) >> (param_w-param_l);
            return h;
        }

        u64 hash_unsigned(long long x) {
            u64 h = (x*param_a) >> (param_w-param_l);
            return h;
        }

        pair<long long,long long> hash_h_and_g(long long x){
            u64 k = hash_unsigned(x);
            long long g = 2*(k & 1)-1;
            long long h = (k >> 1) & (this->param_r-1);
            pair<long long,long long> result = pair<long long, long long>(h,g);
            return result;
        }
};

class CubicHash: public HashFunction {
    protected:
        long long param_a, param_b, param_c, param_d, param_p, param_r;
    public:
        CubicHash(long long p, long long r){
            this->algo = CUBIC_HASH;
            this->param_p = p;
            this->param_r = r;

            random_device rd;
            mt19937_64 rng(rd());
            this->param_a = (rng() % this->param_p);
            this->param_b = (rng() % this->param_p);
            this->param_c = (rng() % this->param_p);
            this->param_d = (rng() % this->param_p);
        }
        
        CubicHash(long long r): CubicHash(DEFAULT_P, r){}

        CubicHash():CubicHash(DEFAULT_P, DEFAULT_R) {}

        long long hash(long long x){
            long long k = (this->param_a * x + this->param_b) % this->param_p;
            k = (k * x + this->param_c) % this->param_p;
            k = (k * x + this->param_d) % this->param_p;
            return k;
        }

        pair<long long,long long> hash_h_and_g(long long x){
            long long k = hash(x);
            long long g = 2*(k & 1)-1;
            long long h = (k >> 1) & (this->param_r-1);

            pair<long long,long long> result = pair<long long, long long>(h,g);
            return result;
        }
};
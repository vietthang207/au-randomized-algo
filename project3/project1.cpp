//#include <bits/stdc++.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

const int N_MAX = 1000;
const int K_MAX = 499;
double p[N_MAX][K_MAX];
double r[N_MAX][K_MAX];

void CreatePricingMatrix(){
    int n = N_MAX;
    int k = K_MAX;
    for(int i = 0; i < k; ++i){
        r[0][i] = 0.25;
    }
    for(int i = 0; i < k; ++i){
        p[0][i] = 0.5;
    }
    for(int i = 1; i < n ; ++i){
        p[i][0] = (1 + r[i-1][0])/2.0;
        r[i][0] = (1 - p[i][0]) * p[i][0] + p[i][0] * r[i-1][0];
    }
    for(int i = 1; i < k; ++i){
        for(int j = 1; j < n; ++j){
            p[j][i] = (1 - r[j-1][i-1] + r[j-1][i])/2.0;
            r[j][i] = (1 - p[j][i]) * (p[j][i] + r[j-1][i-1]) + p[j][i] * r[j-1][i];
        }
    }
    
}

void test(int N, int K){
    double fixedPrice = 1 - (K/(double)N);
    double profit = 0;
    int availableTickets = K;
    int ticketsLeft = 0;
    double profitSum = 0;
    // std::cout << "Number of customers: " << N << std::endl;
    // std::cout << "Number of tickets: " << K << std::endl;
    cout<<K<<","<<N<<",";
    std::random_device rnd;
    std::mt19937 gen(rnd());
    std::uniform_real_distribution<> dist(0.,1.);

    double profits[1000];
    double profitHelp = 0;
    
    for(int j = 0 ; j < 1000; ++j){
        for(int i = 0; i < N; ++i){
            double t = dist(gen);
            if(t >= fixedPrice && availableTickets > 0){
                --availableTickets;
                profit += fixedPrice;
            }
            //std::cout << t << std::endl;
        }
        profits[j] = profit;
        profitSum += profit;
        profit = 0;
        ticketsLeft += availableTickets;
        availableTickets = K;
    }

    for(int i = 0; i < 1000; ++i){
        profitHelp += pow(profits[i] - (profitSum / 1000),2);
    }

    // std::cout << "average profit using fixed price: " << (profitSum / 1000) << std::endl;
    // std::cout << "standard deviation fixed price: " << sqrt(profitHelp/1000) << std::endl;
    // std::cout << "average tickets left using fixed price: " << (ticketsLeft / 1000) << std::endl;
    cout<<profitSum/1000<<","<<sqrt(profitHelp/1000)<<",";

    profitSum = 0;
    ticketsLeft = 0;
    profitHelp = 0;

    for (int i = 0; i < 1000; ++i){
        profit = 0;
        availableTickets = K;
        for(int j = 0; j < N; ++j){
            double t = dist(gen);
            if(t >= p[(N - j) - 1][availableTickets - 1] && availableTickets > 0){
                
                profit += p[(N - j) - 1][availableTickets - 1];
                --availableTickets;
            }
            //std::cout << p[(N - j) - 1][availableTickets - 1] << std::endl;
        }
        profits[i] = profit;
        profitSum += profit;
        ticketsLeft += availableTickets;
        //std::cout << profit << std::endl;
        //std::cout << availableTickets << std::endl;
    }

    for(int i = 0; i < 1000; ++i){
        profitHelp += pow(profits[i] - (profitSum / 1000),2);
    }

    // std::cout << "average profit using optimal price: " << (profitSum / 1000) << std::endl;
    // std::cout << "standard deviation optimal price: " << sqrt(profitHelp/1000) << std::endl;
    // std::cout << "average tickets left using optimal price: " << (ticketsLeft / 1000) << std::endl;
    cout<<profitSum/1000<<","<<sqrt(profitHelp/1000)<<endl;
}

int main(){
    CreatePricingMatrix();
    //std::cout << r[N-1][K-1] << std::endl;
    for(int j = 0; j < K_MAX; ++j){
        for(int i = 0; i < N_MAX ; ++i){
            if(j <= 2){
                //std::cout << p[i][j] << "    ";
                //std::cout << r[i][0] << "    ";
            }
            
        }
        //std::cout << "Hee Ho!" << std::endl;
    }
    cout<<"k,n,fixed_avg,fixed_std_dev,opt_avg,opt_std_dev"<<endl;
    // test(N_MAX,K_MAX);
    int k_values[] = {5, 10, 25, 50, 100, 250};
    int n_values[] = {25, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000};
    for (int i=0; i < size(k_values); i++) {
        int k = k_values[i];
        for (int j=0; j< size(n_values); j++){
            int n = n_values[j];
            if (n>2*k) {
                test(n, k);
            }
        }
    }
    return 0;
}



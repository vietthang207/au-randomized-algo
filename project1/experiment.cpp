//#include <bits/stdc++.h>
//#include <stdio.h>
#include <bit>
#include <bitset>
#include <iostream>
#include <random>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

typedef uint64_t u64;
typedef vector<uint64_t> vu64;

const u64 MAX_U64 = 1UL << 63 - 1 + 1UL << 63;
const int DOMAIN_SIZE = 1 << 25;
const int HASH_PARAM_W = 64;
const int PERFECT_HASH_PARAM_W = 64;
const int PERFECT_HASH_PARAM_L = 25;
const int LINEAR_HASH_PARAM_W = 64;
const int LINEAR_HASH_PARAM_L = 25;
const int UNDEFINED = -1;

random_device rd;
mt19937_64 rng(rd());

u64 round_to_power_of_2(u64 n) {
    n--;
    n |= n >> 1;
    n |= n >> 2;
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n |= n >> 32;
    n++;
    return n;
}

u64 rand_odd_u64(){
    // Return a random unsigned 64-bit odd int
    u64 random_number = (rng()/2)*2+1;
    return random_number;
}

u64 multiply_shift(u64 x, u64 param_a, int param_w, int param_l){
    //multi-shift hash function
    u64 h = (x*param_a) >> (param_w-param_l);
    return h;
}

class Dict {
    public:
        // Construct the dict from an array of elements
        virtual void construct(vu64& a) {
            return;
        }

        // Insert single element, mostly unused.
        virtual bool insert(u64 e) {
            return false;
        }

        virtual bool query(u64 e) {
            return false;
        }

};


class RbTree: public Dict {
    protected:
        set<u64> tree;
    public:
        void construct(vu64& a) {
            tree.insert(a.begin(), a.end());
            return;
        }
        bool insert(u64 e) {
            tree.insert(e);
            return false;
        }
        bool query(u64 e) {
            if (tree.find(e) == tree.end()) {
                return false;
            }
            return true;
        }
};

class PerfectHashTable {
    protected:
        bool is_perfect_hashing(u64 *arr, u64 size, u64 a) {
            for (int i=0; i<size; i++) {
                for (int j = i+1; j<size; j++) {
                    u64 h1 = multiply_shift(arr[i], a, HASH_PARAM_W, param_l);
                    u64 h2 = multiply_shift(arr[j], a, HASH_PARAM_W, param_l);
                    if (h1 == h2) {
                        return false;
                    }
                }
            }
            return true;
        }

        u64 search_perfect_hash_param_a(u64 *arr, u64 size){
            //Take a vector of u64 as input, and return a perfect hash param a
            //A perfect hash function is a hash function with no collision
            u64 a = rand_odd_u64();
            while (!is_perfect_hashing(arr, size, a)) {
                a = rand_odd_u64();
            }
            return a;
        }

        u64 hash(u64 e) {
            u64 h = multiply_shift(e, param_a, HASH_PARAM_W, param_l);
            return h;
        }

    public:
        u64 param_m;
        u64 param_l;
        u64 param_a;
        vu64 hash_table;

        void construct(u64 *arr, u64 size) {
            if (size == 0) return;
            //m=cn^2, l=log(m)
            param_m = bit_ceil(2 * size * size);
            param_l = log2(param_m);
            param_a = search_perfect_hash_param_a(arr, size);
            
            hash_table = vu64(param_m, UNDEFINED);
            for (int i = 0; i < size; i++) {
                u64 h = hash(arr[i]);
                hash_table[h] = arr[i];
            }
        }

        bool query(u64 e) {
            u64 h = hash(e);
            if (hash_table[h] != e) {
                return false;
            }
            return true;
        }
};

class DynamicHashTable: public Dict {
    //TODO implement dynamic perfect hash
    protected:
        u64 param_m;
        u64 param_l;
        u64 param_a;

        vu64 bucket_sizes;
        vector<u64*> buckets;
        vector<PerfectHashTable*> perfect_hash_tables;


        bool is_linear_hashing(vu64& arr, u64 a) {
            vector<int> bucket_sizes(param_m, 0);
            for (int i = 0; i < arr.size(); i++) {
                u64 h = multiply_shift(arr[i], a, HASH_PARAM_W, param_l); 
                bucket_sizes[h]++;
            }
            int sum = 0;
            for (int i = 0; i < bucket_sizes.size(); i++){
                sum += bucket_sizes[i] * bucket_sizes[i];
            }
            if (sum < 2 * arr.size()) {
                return true;
            }
            return false;
        }

        u64 search_linear_hash_param_a(vu64& arr) {
            //Take a vector of u64 as input, and return a linear hash param a
            //A linear hash function is a hash function with linear size, and sum of squared of bucket size is also less than 2n
            u64 a = rand_odd_u64();
            while (!is_linear_hashing(arr, a)) {
                a = rand_odd_u64();
            }
            return a;
        }

        u64 hash(u64 e) {
            u64 h = multiply_shift(e, param_a, HASH_PARAM_W, param_l);
            return h;
        }

    public:
        void construct(vu64& arr) {
            //m = 2cn, l = log(m)
            param_m = bit_ceil(4 * arr.size());
            param_l = log2(param_m);

            auto start = clock();
            param_a = search_linear_hash_param_a(arr);
            auto stop = clock();
            // cout << "search linear : "<<stop - start<<endl;
            start = clock();

            bucket_sizes = vu64(param_m, 0);
            for (int i = 0; i < arr.size(); i++) {
                u64 h = multiply_shift(arr[i], param_a, HASH_PARAM_W, param_l); 
                bucket_sizes[h]++;
            }
            stop = clock();
            // cout << "compute size  : "<<stop - start<<endl;
            start = clock();

            for (int i = 0; i < param_m; i++) {
                if (bucket_sizes[i] == 0){
                    buckets.push_back(nullptr);
                } else {
                    u64 *b = new u64[bucket_sizes[i]];
                    buckets.push_back(b);
                }
            }
            stop = clock();
            // cout << "initialize    : "<<stop - start<<endl;
            start = clock();

            vu64 bucket_indices = vu64(param_m, 0);
            for (int i = 0; i < arr.size(); i++) {
                u64 h = multiply_shift(arr[i], param_a, HASH_PARAM_W, param_l);
                buckets[h][bucket_indices[h]] = arr[i];
                bucket_indices[h]++;
            }
            stop = clock();
            // cout << "build buckets : "<<stop - start<<endl;
            start = clock();

            for (int i = 0; i < param_m; i++) {
                if (bucket_sizes[i] < 2) {
                    perfect_hash_tables.push_back(nullptr);
                } else {
                    PerfectHashTable *pht = new PerfectHashTable;
                    pht -> construct(buckets[i], bucket_sizes[i]);
                    perfect_hash_tables.push_back(pht);
                }
            }
            stop = clock();
            // cout << "search perfect: "<<stop - start<<endl;
            return;
        }

        bool insert(u64 e) {
            return false;
        }
        bool query(u64 e) {
            u64 h = hash(e);
            if (bucket_sizes[h] == 0) {
                return false;
            } else if (bucket_sizes[h] == 1) {
                return buckets[h][0] == e;
            } else {
                return perfect_hash_tables[h] -> query(e);
            }
        }
};

class ChainingHashTable: public Dict {
    //TODO implement hashing with chaining
    protected:
        u64 param_m;
        u64 param_l;
        u64 param_a;
        u64 max_bucket_size;

        vu64 bucket_sizes;
        vector<u64*> buckets;


        u64 hash(u64 e) {
            u64 h = multiply_shift(e, param_a, HASH_PARAM_W, param_l);
            return h;
        }

    public:
        void construct(vu64& arr) {
            max_bucket_size = 0;
            //m = 2cn, l = log(m)
            param_m = bit_ceil(4 * arr.size());
            param_l = log2(param_m);

            auto start = clock();
            param_a = rand_odd_u64();
            auto stop = clock();
            // cout << "search linear : "<<stop - start<<endl;
            start = clock();

            bucket_sizes = vu64(param_m, 0);
            for (int i = 0; i < arr.size(); i++) {
                u64 h = multiply_shift(arr[i], param_a, HASH_PARAM_W, param_l); 
                bucket_sizes[h]++;
            }
            stop = clock();
            // cout << "compute size  : "<<stop - start<<endl;
            start = clock();

            for (int i = 0; i < param_m; i++) {
                max_bucket_size = max(max_bucket_size, bucket_sizes[i]);
                if (bucket_sizes[i] == 0){
                    buckets.push_back(nullptr);
                } else {
                    u64 *b = new u64[bucket_sizes[i]];
                    buckets.push_back(b);
                }
            }
            stop = clock();
            // cout << "initialize    : "<<stop - start<<endl;
            start = clock();

            vu64 bucket_indices = vu64(param_m, 0);
            for (int i = 0; i < arr.size(); i++) {
                u64 h = multiply_shift(arr[i], param_a, HASH_PARAM_W, param_l);
                buckets[h][bucket_indices[h]] = arr[i];
                bucket_indices[h]++;
            }
            stop = clock();
            // cout << "build buckets : "<<stop - start<<endl;
            start = clock();
            return;
        }

        bool insert(u64 e) {
            return false;
        }
        bool query(u64 e) {
            u64 h = hash(e);
            if (bucket_sizes[h] == 0) {
                return false;
            } else {
                for (int i = 0; i < bucket_sizes[h]; i++) {
                    if (buckets[h][i] == e) return true;
                }
                return false;
            }
        }
        u64 get_longest_chain_size(){
            return max_bucket_size;
        }
};

struct data_point {
    int logN;

    int dht_construction_time;
    int dht_query_time;

    int cht_construction_time;
    int cht_query_time;
    int longest_chain;

    int rb_tree_construction_time;
    int rb_tree_query_time;
};

vector<data_point> run_test(vu64& arr, int logN) {
    vector<data_point> result;
    shuffle(arr.begin(), arr.end(), rng);

    for (int i = logN; i < logN + 1; i++) {
        int n = 1 << i;
        vu64 sub_arr(arr.begin(), arr.begin() + n);
        // cout << "logN: " << i << " " << " " << sub_arr.size() << endl;
        
        DynamicHashTable dht;
        auto start = clock();
        dht.construct(sub_arr);
        auto stop = clock();
        auto dht_construction_time = stop - start;

        start = clock();
        for (int j = 0; j < n; j++) {
            bool query_res = dht.query(sub_arr[j]);
            if (!query_res) {
                cout << "Error: key "<<sub_arr[j]<<" not found"<<endl;
            }
        }
        stop = clock();
        auto dht_query_time = stop - start;

        ChainingHashTable cht;
        start = clock();
        cht.construct(sub_arr);
        stop = clock();
        auto cht_construction_time = stop - start;

        start = clock();
        for (int j = 0; j < n; j++) {
            bool query_res = cht.query(sub_arr[j]);
            if (!query_res) {
                cout << "Error: key "<<sub_arr[j]<<" not found"<<endl;
            }
        }
        stop = clock();
        auto cht_query_time = stop - start;

        RbTree rb_tree;
        start = clock();
        rb_tree.construct(sub_arr);
        stop = clock();
        auto rb_construction_time = stop - start;

        start = clock();
        for (int j = 0; j < n; j++) {
            bool query_res = rb_tree.query(sub_arr[j]);
            if (!query_res) {
                cout << "Error: key "<<sub_arr[j]<<" not found"<<endl;
            }
        }
        stop = clock();
        auto rb_query_time = stop - start;

        // cout << dht_construction_time << " microseconds to construct Dynamic Hash" << endl;
        // cout << cht_construction_time << " microseconds to construct Chaining Hash" << endl;
        // cout << rb_construction_time << " microseconds to construct Rb tree" << endl;
        // cout << dht_query_time << " microseconds to query Dynamic Hash" << endl;
        // cout << cht_query_time << " microseconds to query Chaining Hash" << endl;
        // cout << rb_query_time << " microseconds to query Rb tree" << endl;
        // cout << "Longest chain: " << cht.get_longest_chain_size()<<endl;

        data_point d;
        d.logN = i;
        d.dht_construction_time = dht_construction_time;
        d.dht_query_time = dht_query_time;
        d.cht_construction_time = cht_construction_time;
        d.cht_query_time = cht_query_time;
        d.longest_chain = cht.get_longest_chain_size();
        d.rb_tree_construction_time = rb_construction_time;
        d.rb_tree_query_time = rb_query_time;

        result.push_back(d);
    }
    return result;
}

int main(int argc, char* argv[]){
	ios::sync_with_stdio(0);
    vu64 arr;
    for (int i = 0; i < pow(2, 24); i++) {
        arr.push_back(i * 100);
    }

    int logN = atoi(argv[1]);
    int num_test = 5;
    vector<data_point> data[num_test];
    for (int i = 0; i < num_test; i++) {
        // cout<<"run_test "<<i<<endl;
        data[i] = run_test(arr, logN);
    }

    vector<data_point> average(data[0].size());
    for (int i = 0; i < average.size(); i++) {
        for (int j = 0; j < num_test; j++) {
            average[i].logN = data[j][i].logN;
            average[i].dht_construction_time += data[j][i].dht_construction_time;
            average[i].dht_query_time += data[j][i].dht_query_time;
            average[i].cht_construction_time += data[j][i].cht_construction_time;
            average[i].cht_query_time += data[j][i].cht_query_time;
            average[i].longest_chain += data[j][i].longest_chain;
            average[i].rb_tree_construction_time += data[j][i].rb_tree_construction_time;
            average[i].rb_tree_query_time += data[j][i].rb_tree_query_time;
        }

        average[i].dht_construction_time /= num_test;
        average[i].dht_query_time /= num_test;
        average[i].cht_construction_time /= num_test;
        average[i].cht_query_time /= num_test;
        average[i].longest_chain /= num_test;
        average[i].rb_tree_construction_time /= num_test;
        average[i].rb_tree_query_time /= num_test;

        // cout << "logN: " << average[i].logN<<endl;
        // cout << average[i].dht_construction_time << " microseconds to construct Dynamic Hash" << endl;
        // cout << average[i].cht_construction_time << " microseconds to construct Chaining Hash" << endl;
        // cout << average[i].rb_tree_construction_time << " microseconds to construct Rb tree" << endl;
        // cout << average[i].dht_query_time << " microseconds to query Dynamic Hash" << endl;
        // cout << average[i].cht_query_time << " microseconds to query Chaining Hash" << endl;
        // cout << average[i].rb_tree_query_time << " microseconds to query Rb tree" << endl;
        // cout << "Longest chain: " << average[i].longest_chain<<endl;
    }

    // cout <<"logN, dht_construction, cht_construction, rb_tree_construction, dht_query, cht_query, rb_tree_query, longest_chain"<<endl;
    for (int i = 0; i < average.size(); i++) {
        cout<<average[i].logN<<", "<<average[i].dht_construction_time<<", "<<average[i].dht_query_time<<", "<<average[i].cht_construction_time<<", "<<average[i].cht_query_time<<", "<<average[i].rb_tree_construction_time<<", "<<average[i].rb_tree_query_time<<", "<<average[i].longest_chain<<endl;
    }

    return 0;
}




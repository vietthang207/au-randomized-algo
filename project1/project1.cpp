//#include <bits/stdc++.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

typedef uint64_t u64;
typedef vector<uint64_t> vu64;

u64 rand_u64(){
    random_device rd;
    mt19937_64 rng(rd());
    uniform_int_distribution<u64> dis;
    return dis(rng);
}

u64 multiply_shift(u64 x, u64 a, int w, int l){
    //multi-shift hash function
    return (a*x) >> (w-l);
}

u64 search_perfect_hash_param(vu64 arr){
    //Take a vector of u64 as input, and return a perfect hash (non-collision hash) param a, with w=l=64 bits
    return 0;
}

class Dict {
    public:
        // Construct the dict from an array of elements
        virtual void construct(vu64 a) {
            return;
        }

        // Insert single element, mostly unused.
        virtual bool insert(u64 e) {
            return false;
        }

        virtual bool query(u64 e) {
            return false;
        }

};


class RbTree: public Dict {
    protected:
        set<u64> tree;
    public:
        void construct(vu64 a) {
            tree.insert(a.begin(), a.end());
            return;
        }
        bool insert(u64 e) {
            tree.insert(e);
            return false;
        }
        bool query(u64 e) {
            if (tree.find(e) == tree.end()) {
                return false;
            }
            return true;
        }
};

class DynamicHashTable: public Dict {
    //TODO implement dynamic perfect hash
    public:
        void construct(vu64 a) {
            return;
        }
        bool insert(u64 e) {
            return false;
        }
        bool query(u64 e) {
            return false;
        }
};

class ChainingHashTable: public Dict {
    protected:
        u64 a;
        int w;
        int l;
        vu64* list;
    public:
        void construct(vu64 a, int n) {
            random_device rd;
            mt19937 rng(rd());
            this->w = 64;
            this->l = n;
            this->a = rng();
            while(this->a % 2 != 1)
            {
                this->a = rng();
            }
            int s = pow(2,n);
            this->list = new vu64[s]();
            for(int i = 0; i < w; ++i)
            {
                this->insert(a[i]);
            }
            return;
        }
        bool insert(u64 e) {
            int temp = multiply_shift(e,this->a,this->w,this->l);
            list[temp].push_back(e);
            return false;
        }
        bool query(u64 e) {
            int temp = multiply_shift(e,this->a,this->w,this->l);
            if(find(this->list[temp].begin(),this->list[temp].end(),e) != this->list[temp].end())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        int longest_chain()
        {
            int s = pow(2,this->l);
            int max_chain_length = 0;
            for(int i = 0; i < s; ++i)
            {
                
                if(this->list[i].size() > max_chain_length)
                {
                    
                    max_chain_length = this->list[i].size();
                }
            }
            return max_chain_length;
        }
};

int main(){
	ios::sync_with_stdio(0);
    cout << "Hello World!\n";
    vu64 arr;
    for (int i = 0; i < pow(2, 24); i++) {
        arr.push_back(i * 100);
    }
    random_device rd;
    mt19937 rng(rd());
    shuffle(arr.begin(), arr.end(), rng);

    ofstream myfile;
    myfile.open ("results.txt");
    
    for (int i = 5; i < 25; i++) {
        int n = pow(2, i);
        vu64 sub_arr(arr.begin(), arr.begin() + n);
        cout << i << " " << " " << sub_arr.size() << endl;
        
        ChainingHashTable chain_table;
        auto start = clock();
        chain_table.construct(sub_arr, i);
        auto stop = clock();
        auto construction_time = stop - start;
        myfile << "Construction time: " << construction_time << " microseconds" << endl;
        cout << "longest chain: " << chain_table.longest_chain() << endl;
        //myfile << "longest chain: " << chain_table.longest_chain() << endl;
        start = clock();
        for(int j = 0; j < i; j ++)
        {
            chain_table.query(sub_arr[j]);
        }
        stop = clock();
        construction_time = stop - start;
        myfile << "Query time: " << construction_time << " microseconds" << endl;
    }
    for (int i = 5; i < 25; i++) {
        int n = pow(2, i);
        vu64 sub_arr(arr.begin(), arr.begin() + n);
        cout << i << " " << " " << sub_arr.size() << endl;
        
        RbTree rb_tree;
        auto start = clock();
        rb_tree.construct(sub_arr);
        auto stop = clock();
        auto construction_time = stop - start;
        myfile << "Construction time: " << construction_time << " microseconds" << endl;
        start = clock();
        for(int j = 0; j < i; j ++)
        {
            rb_tree.query(sub_arr[j]);
        }
        stop = clock();
        construction_time = stop - start;
        myfile << "Query time: " << construction_time << " microseconds" << endl;
    }
    myfile.close();
    return 0;
}




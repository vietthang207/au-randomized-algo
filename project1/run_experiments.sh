#!/bin/bash

echo "logN, dht_construction, dht_query, cht_construction, cht_query, rb_tree_construction, rb_tree_query, longest_chain"
g++ -std=c++20 experiment.cpp
for i in {5..24}
do
    ./a.out $i
done